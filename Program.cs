﻿using System;
//leva cartella c
//nooooooo
//siiiiii
//dajeeee
namespace esercizi2_csharp
{
    class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine("Hello World!");
            Person persona1 = new Person();
            Person persona2 = new Person();
            Person persona3 = new Person();
            Actor actor1 = new Actor();
            Character character1 = new Character();
            character1.Name="bradd";
            character1.Surname="pitt";
            character1.role="antagonista";
            character1.subrole="niente";
            character1.Year=1980;
            actor1.Name="leonardo";
            actor1.Surname="di caprio";
            actor1.Year=1970;
            actor1.role="protagonista";
            persona1.Name= "pippo";
            persona1.Surname= "baudo";
            persona1.Year= 1974;
            persona2.Name="mister";
            persona2.Surname="topolino";
            persona2.Year= 1977;
            persona3.Name="alex";
            persona3.Surname="del piero";
            persona3.Year= 1984;
            Console.WriteLine(persona1.NomeCompleto());
            Console.WriteLine(persona2.NomeCompleto());
            Console.WriteLine(persona3.NomeCompleto());
            Console.WriteLine(actor1.RolePlaying());
            Console.WriteLine(actor1.NomeCompleto());
            Console.WriteLine(character1.RolePlaying());
            Console.WriteLine(persona1.StampaMessaggio(" Ciao "));
            Console.WriteLine(actor1.StampaMessaggio(" Daje "));
        }
    }
    class Person
    {
        private string _name;
        public string Name{
                    get=>_name;
                    set=>_name=value+" Svegliate ";
        }
        public string Surname{
                    get;
                    set;
        }
        public int Year{
                    get;
                    set;
        }
        protected int età(){
            return (2021-Year);
        }
        public string NomeCompleto(){
            return Name+" "+Surname+" "+Year+" "+età();
        }
        public string StampaMessaggio(string messaggino){
            return "questo è il metodo stampa messaggio di Person"+messaggino;
        }
    }

    class Actor:Person
    {
        public string role {
                get;
                set;
        }
        public string RolePlaying(){
            return "the actor name is "+Name+" "+Surname+" "+role+" "+età();
        }
        /*public override string StampaMessaggio(){
                return "questo è il metodo stampa messaggio di Actor";
            }*/

    }

    class Character:Actor
    {
        public string subrole {
            get;
            set;
        }
        public new string RolePlaying(){
            return "the actor name is "+Name+" "+Surname+" "+role+" "+subrole+" "+età();
        }

    }
}

